#ifndef APPLOGGER_H
#define APPLOGGER_H

#include <QObject>

class AppLogger : public QObject
{
    Q_OBJECT
public:
    explicit AppLogger(const QString prefix = "AppLogger", QObject *parent = nullptr);
    ~AppLogger();

public:
    bool writeLog(const QString &log);
    bool writeLog(const QByteArray &log);
    QString byteArrayToString(const QByteArray &ary);

signals:

private:
    QString m_filePath;
    QString m_prefix;
};

#endif // APPLOGGER_H

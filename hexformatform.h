#ifndef HEXFORMATFORM_H
#define HEXFORMATFORM_H

#include <QMainWindow>
#include <QUrl>
#include <QThread>

class HexFormater;

QT_BEGIN_NAMESPACE
namespace Ui { class HexFormatForm; }
QT_END_NAMESPACE


class HexFormatForm : public QMainWindow
{
    Q_OBJECT

public:
    HexFormatForm(QWidget *parent = nullptr);
    ~HexFormatForm();

signals:
    void onSigFileObjectReceivced(const QString& filePath);

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);

public slots:
    void setFileFormatCheckStatus(const QString& status);
    void applyMethodOneFileDecode(const QList<QUrl> &urlList);
    void applyMethodTwoFileDecode(const QList<QUrl> &urlList);

private:
    Ui::HexFormatForm *ui;
    HexFormater *m_hex_formater;
    QThread *m_thrd;
};

#endif // HEXFORMATFORM_H

#include <QDragEnterEvent>
#include <QMimeData>
#include <QDateTime>
#include <QFileInfo>
#include <QUrl>
#include <QList>

#include "hexformatform.h"
#include "hexformater.h"
#include "applogger.h"
#include "ui_hexformatform.h"

static AppLogger _s_app_logger_;

HexFormatForm::HexFormatForm(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::HexFormatForm)
{
    ui->setupUi(this);
    ui->cfgFileTE->setAcceptDrops(false);
    setAcceptDrops(true);

    m_hex_formater = new HexFormater(this);

    connect(this, SIGNAL(onSigFileObjectReceivced(const QString&)), m_hex_formater, SLOT(onReceiveFileCfgPath(const QString&)));
    connect(m_hex_formater, SIGNAL(sigOnFileFormatCheckStatus(const QString&)), this, SLOT(setFileFormatCheckStatus(const QString&)));
}

HexFormatForm::~HexFormatForm()
{
    delete ui;
}

void HexFormatForm::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasUrls())
    {
        event->acceptProposedAction();
    }
    else
    {
        event->ignore();
    }
}

void HexFormatForm::dropEvent(QDropEvent *event)
{
    const QMimeData *mimeData = event->mimeData();
    if (mimeData->hasUrls())
    {
        QList<QUrl> urlList = mimeData->urls();
        if (urlList.isEmpty()) return;

        if (urlList.length() == 1)
        {
            applyMethodOneFileDecode(urlList);
        }
        else if (urlList.length() == 2)
        {
            applyMethodTwoFileDecode(urlList);
        }
        else
        {
            ui->cfgFileTE->setText(QDateTime::currentDateTime().toString(" #yyyy-MM-dd_hh:mm:ss#") + ", too many files, not support yet!");
        }
    }
}

void HexFormatForm::applyMethodOneFileDecode(const QList<QUrl> &urlList)
{
    if (urlList.isEmpty()) return;

    QString fileName = urlList.at(0).toLocalFile();
    if (!fileName.isEmpty())
    {
        QFile file(fileName);
        QFileInfo fileInfo = QFileInfo(fileName);
        if (file.exists() && fileInfo.completeSuffix().toLower().contains("cfg"))
        {
            _s_app_logger_.writeLog(QString("a new file drop in, file:#%1#!").arg(fileName));
            ui->cfgFileTE->setText(fileName + QDateTime::currentDateTime().toString(" #yyyy-MM-dd_hh:mm:ss#"));
            file.open(QIODevice::ReadOnly);
            ui->cfgFileTE->append(file.readAll());
            file.close();
            emit this->onSigFileObjectReceivced(fileName);
        }
        else
        {
            ui->cfgFileTE->setText("file format not support!");
            _s_app_logger_.writeLog(QString("file:#%1# format not support!").arg(fileName));
        }
    }
}

void HexFormatForm::applyMethodTwoFileDecode(const QList<QUrl> &urlList)
{
    if (urlList.isEmpty()) return;

    int len = urlList.length();
    for (int i = 0; i < len; ++i)
    {
        QString fileName = urlList.at(i).toLocalFile();
        if (!fileName.isEmpty())
        {
            QFile file(fileName);
            QFileInfo fileInfo = QFileInfo(fileName);
            if (file.exists() && fileInfo.completeSuffix().toLower().contains("cfg"))
            {
                _s_app_logger_.writeLog(QString("a new file drop in, file:#%1#!").arg(fileName));
                ui->cfgFileTE->setText(fileName + QDateTime::currentDateTime().toString(" #yyyy-MM-dd_hh:mm:ss#"));
                file.open(QIODevice::ReadOnly);
                ui->cfgFileTE->append(file.readAll());
                file.close();
                emit this->onSigFileObjectReceivced(fileName);
            }
            else
            {
                ui->cfgFileTE->setText("file format not support!");
                _s_app_logger_.writeLog(QString("file:#%1# format not support!").arg(fileName));
            }
        }
    }
}

void HexFormatForm::setFileFormatCheckStatus(const QString& status)
{
    QString strStatusBuf = QString("\n%1#%2\n");
    QString strDateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd_hh:mm:ss");
    ui->cfgFileTE->setText(QString("#%1# %2\n").arg(strDateTime).arg(status) + ui->cfgFileTE->toPlainText());
}

#ifndef HEXFORMATER_H
#define HEXFORMATER_H

#include <QObject>
#include <QList>

class HexFormater : public QObject
{
    Q_OBJECT
public:
    explicit HexFormater(QObject *parent = nullptr);
    ~HexFormater();

signals:
    void sigOnFileFormatCheckStatus(const QString& strStatus);

public slots:
    void onReceiveFileCfgPath(const QString& filePath);

public:
    void setFilePath(const QString& filePath) { m_file_path = filePath; }
    QString getFilePath() const { return m_file_path; }
    QString appendHexStringLineToDataGroup(const QString& hexStrLine);
    void generateModeOneHexStringToBuffer(const QString& strKey);
    void generateModeTwoHexStringToBuffer(const QString& strKey);
    bool generateHexGroupStringFmt(const QList<QString>& strListKey, const QMap<QString, QStringList*>& strMapKeyList);

protected:
    QString getHexCodeStrFromGroupStr(const QString& strGroupBuf, const int begIdx);
    QString getPageHexIdFromGroupStr(const QString& strGroupBuf, const int begIdx);
    QStringList& mergeSortGroupByData(const QStringList& strFullEQList, const QStringList& strPartEQList);

private:
    void resetListAndMapData();
    bool isTheSameSubGroupKey(const QString& subGroupKeyOne, const QString& subGroupKeyTwo) const;
    bool checkFileDataIsLegal(const QString& filePath, int *line);
    int getGroupHexItemLength(const QString& strGroupStr) { return strGroupStr.count("0x") - 2; };
    bool isGroupItemKeyMapModeOne(const QString& strKeyGroup, const QList<QString>& strListKey, const QMap<QString, QStringList*>& strMapKeyList);
    void formatStrDataAndSaveToLocal(const QString& filePath, const QStringList& strDataGroupLst);
    QString splitStrGroupToSpecifiyWidth(const QString& strDataGroup);
    bool isFrontThreeItemDiffer(const QString& strA, const QString& strB) const;

private:
    QString m_file_path;
    QStringList m_file_list;
    int m_cur_mode;
    QString m_dst_file_path;
    QString m_str_key;
};

#endif // HEXFORMATER_H

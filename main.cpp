#include <QApplication>

#include "hexformatform.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HexFormatForm w;
    w.show();
    return a.exec();
}

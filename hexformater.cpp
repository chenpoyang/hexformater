#include <QStringList>
#include <QDateTime>
#include <QIODevice>
#include <QFileInfo>
#include <QMessageBox>
#include <QMap>
#include <QtMath>
#include <QTextStream>
#include <QDebug>

#define _BUF_MAX_SIZE_ 1024

#include "applogger.h"

static QMap<QString, QStringList*> _s_map_key_str_;
static QMap<QString, int> _s_mode_id_;
static QList<QString> _s_list_key_str_;
static QStringList _s_str_dst_mode_;
static AppLogger _s_logger_;

#include "hexformater.h"

HexFormater::HexFormater(QObject *parent) : QObject(parent)
{
}

HexFormater::~HexFormater()
{
    int lenKey = _s_list_key_str_.length();
    QString strKey;
    QStringList *pStrList = NULL;
    for (int i = 0; i < lenKey; ++i)
    {
        strKey = _s_list_key_str_.at(i);
        if (_s_map_key_str_.contains(strKey))
        {
            pStrList = _s_map_key_str_[strKey];
            delete pStrList;
            pStrList = NULL;
            _s_map_key_str_.remove(strKey);
        }
    }
    _s_str_dst_mode_.clear();
    _s_mode_id_.clear();
}

QString HexFormater::appendHexStringLineToDataGroup(const QString& hexStrLine)
{
    if (hexStrLine.trimmed().remove("\r").remove("\n").trimmed().length() <= 0)
    {
        return QString("");
    }

    QStringList *strList = NULL;
    if (hexStrLine.startsWith("#"))
    {
        if (!_s_map_key_str_.contains(hexStrLine.trimmed()))
        {
            strList = new QStringList;
            _s_map_key_str_.insert(hexStrLine.trimmed(), strList);
            _s_list_key_str_.append(hexStrLine.trimmed());
        }
        return QString("");
    }

    if (_s_list_key_str_.length() <= 0)
    {
        QString strCustKey = QDateTime::currentDateTime().toString("#yyyy-MM-dd_hh_mm_ss");
        if (!_s_map_key_str_.contains(strCustKey))
        {
            strList = new QStringList;
            _s_map_key_str_.insert(strCustKey.trimmed(), strList);
            _s_list_key_str_.append(strCustKey.trimmed());
        }
        return QString("");
    }

    m_str_key = _s_list_key_str_.back();
    strList = _s_map_key_str_[m_str_key];

    QString strBuf = hexStrLine.trimmed().remove("\r").remove("\n");
    strList->append(strBuf);

    return QString("");
}

void HexFormater::resetListAndMapData()
{
    int lenKey = _s_list_key_str_.length();
    QString strKey;
    QStringList *pStrList = NULL;
    for (int i = 0; i < lenKey; ++i)
    {
        strKey = _s_list_key_str_.at(i);
        if (_s_map_key_str_.contains(strKey))
        {
            pStrList = _s_map_key_str_[strKey];
            delete pStrList;
            pStrList = NULL;
            _s_map_key_str_.remove(strKey);
        }
    }

    _s_map_key_str_.clear();
    _s_list_key_str_.clear();
    _s_str_dst_mode_.clear();
    _s_mode_id_.clear();
}

/*
#initialization script
w 58 00 00
w 58 04 c0
w 58 00 00
w 58 fc 86
w 58 fd 25
w 58 fe 15
w 58 00 01
w 58 02 20
w 58 00 00
w 58 00 00
w 58 00 00
w 58 00 00
w 58 00 00
*/
void HexFormater::generateModeOneHexStringToBuffer(const QString& strKey)
{
    QStringList *pStrList;

    QString strBuf = "";
    if (_s_list_key_str_.contains(strKey.trimmed()) && _s_map_key_str_.contains(strKey.trimmed()))
    {
        pStrList = _s_map_key_str_[strKey.trimmed()];
        int len;
        len = pStrList->length();
        strBuf = "{ ";
        for (int i = 0; i < len; ++i)
        {
            QStringList strListTmp = pStrList->at(i).split(" ");
            if (strListTmp.length() >= 2)
            {
                int idxLen = strListTmp.length();
                int idx1 = idxLen - 2, idx2 = idxLen - 1;
                strBuf += QString("0x%1, 0x%2").arg(strListTmp.at(idx1)).arg(strListTmp.at(idx2));
            }
            if (i != len - 1)
            {
                strBuf += QString(", ");
            }
            else
            {
                strBuf += QString(" }");
            }
        }
    }

    QString strGroupItemLen = QString::number(strBuf.count("0x") - 2);
    QString strKeyBuf = strKey;
    strKeyBuf = strKeyBuf.replace("#", "//");
    _s_logger_.writeLog(strKeyBuf);
    _s_str_dst_mode_.append(strKeyBuf);
    strBuf = strBuf.replace("{ ", QString("{ %1, ").arg(strGroupItemLen));
    _s_logger_.writeLog(strBuf);
    _s_str_dst_mode_.append(strBuf);
}

/*
#EQ
w 58 00 06
w 58 b0 08 00 00 00
w 58 00 06
w 58 b4 f1 7b 20 b3
w 58 00 06
w 58 b8 06 a4 f1 ec
w 58 00 06
w 58 bc 0e 84 df 4d
w 58 00 06
w 58 c0 f9 5b 0e 14

{ 20, 0x06, 0xb0, x08, 0x00, 0x00, 0x00, 0xf1, 0x7b, 0x20, 0xb3, 0x06, 0xa4, 0xf1, 0xec, 0x0e, 0x84, 0xdf, 0x4d, 0xf9, 0x5b, 0x0e, 0x14 }

w 58 00 07
w 58 e0 08 00 00 00
w 58 00 07
w 58 e4 f1 7b 20 b3
w 58 00 07
w 58 e8 06 a4 f1 ec
w 58 00 07
w 58 ec 0e 84 df 4d
w 58 00 07
w 58 f0 f9 5b 0e 14
*/
void HexFormater::generateModeTwoHexStringToBuffer(const QString& strKey)
{
    QStringList strGroupList;
    QStringList *pStrList = NULL;
    QString strBuff = "";
    static QStringList strListBuf;

    strListBuf.clear();
    if (_s_list_key_str_.contains(strKey.trimmed()) && _s_map_key_str_.contains(strKey))
    {
        pStrList = _s_map_key_str_[strKey.trimmed()];
        int len;
        len = pStrList->length();
        QString curStrGroupId;;
        QString curStrPageBuf;
        QString preStrGroupId, preStrPageBuf;
        int i = 0;
        QString strDstGroupBuf = "";
        QString strPageHexId = "";
        while (i < len)
        {
            curStrGroupId = curStrPageBuf = "";
            if (i % 2 == 0)
            {
                curStrGroupId = pStrList->at(i);
            }

            if (i + 1 < len)
            {
                curStrPageBuf = pStrList->at(i + 1);
            }

            if (isTheSameSubGroupKey(preStrGroupId, curStrGroupId))
            {
                strDstGroupBuf = strDstGroupBuf + ", " +  getHexCodeStrFromGroupStr(curStrPageBuf, 3);
            }
            else
            {
                if (preStrGroupId.trimmed().length() <= 0)
                {
                    QString strHexPageId;
                    strHexPageId = getPageHexIdFromGroupStr(curStrGroupId, 2);
                    strDstGroupBuf = strHexPageId + ", " + getHexCodeStrFromGroupStr(curStrPageBuf, 2);
                }
                else
                {
                    QString strHexPageId;

                    strListBuf.append(strDstGroupBuf);
                    strDstGroupBuf = getHexCodeStrFromGroupStr(curStrPageBuf, 2);
                    if (curStrGroupId.split(" ").length() >= 3)
                    {
                        strHexPageId = getPageHexIdFromGroupStr(curStrGroupId, 2);
                        strDstGroupBuf = strHexPageId + ", " + strDstGroupBuf;
                    }
                    else
                    {
                        strHexPageId = "";
                    }
                }
            }

            i += 2;
            preStrGroupId = curStrGroupId;
            preStrPageBuf = curStrPageBuf;
        }
        strListBuf.append(strDstGroupBuf);
    }

    int len = strListBuf.length();
    QString strKeyBuf = strKey;
    QString strGroupBuf;
    QString strLogBuf;
    strKeyBuf = strKeyBuf.replace("#", "//");
    _s_logger_.writeLog(strKeyBuf);
    _s_str_dst_mode_.append(strKeyBuf);
    QString strGroupItemLen;
    for (int i = 0; i < len; ++i)
    {
        strGroupBuf = strListBuf.at(i);
        strLogBuf = strGroupBuf;

        strGroupItemLen = QString::number(getGroupHexItemLength(strLogBuf));
        if (i + 1 != len)
        {
            strLogBuf = "{ " + strGroupItemLen + ", " + strLogBuf + " }, ";
        }
        else
        {
            strLogBuf = "{ " + strGroupItemLen + ", " + strLogBuf + " }";
        }
        _s_logger_.writeLog(strLogBuf);
        _s_str_dst_mode_.append(strLogBuf);
    }

}

bool HexFormater::isTheSameSubGroupKey(const QString& subGroupKeyOne, const QString& subGroupKeyTwo) const
{
    return subGroupKeyOne.trimmed().compare(subGroupKeyTwo.trimmed()) == 0 ? true : false;
}

QString HexFormater::getHexCodeStrFromGroupStr(const QString& strGroupBuf, const int begIdx)
{
    QStringList strBufList;

    strBufList = strGroupBuf.split(" ");
    QString strBuf = "";
    int len = strBufList.length();
    for (int i = begIdx; i < len; ++i)
    {
        if (i + 1 == len)
        {
            strBuf += QString("0x%1").arg(strBufList.at(i));
        }
        else
        {
            strBuf += QString("0x%1, ").arg(strBufList.at(i));
        }
    }

    return strBuf;
}

QString HexFormater::getPageHexIdFromGroupStr(const QString& strGroupBuf, const int begIdx)
{
    QStringList strBufList;

    static QString _s_16_ch = "0123456789abcdef";
    static QString _s_16_CH = "0123456789ABCDEF";

    strBufList = strGroupBuf.split(" ");
    QString strBuf = "";
    int len = strBufList.length();
    for (int i = begIdx; i < len; ++i)
    {
        strBuf += strBufList.at(i);
    }

    strBuf = strBuf.trimmed();
    len = strBuf.length();
    qint64 num = 0, idxBase;
    QChar ch;
    for (int i = len - 1; i >= 0; --i)
    {
        ch = strBuf.at(i).toLower();
        if (ch >= '0' && ch <= '9')
        {
            idxBase = ch.toLatin1() - '0';
        }
        else
        {
            idxBase = 10 + (ch.toLatin1() - 'a');
        }
        num += idxBase * qPow(16, len - i - 1);
    }

    QString hexNum = QString("%1").arg(QString::number(num, 16));
    if (hexNum.length() <= 1)
    {
        hexNum = "0x0" + hexNum;
    }
    else
    {
        hexNum = "0x" + hexNum;
    }

    return hexNum;
}

void HexFormater::onReceiveFileCfgPath(const QString& filePath)
{
    m_file_path = filePath;
    QFileInfo fileInfo = QFileInfo(m_file_path);
    m_dst_file_path = filePath;
    QString fileSuffix = "." + fileInfo.completeSuffix();
    m_dst_file_path = m_dst_file_path.remove(fileSuffix);
    m_dst_file_path = QString("%1_%2.txt").arg(m_dst_file_path).arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    QString strLog = QString("file to be saved:|%1|.").arg(m_dst_file_path);
    _s_logger_.writeLog(strLog);
    QString strLogBuf;

    qDebug() << "HexFormater:" + m_file_path;

    _s_map_key_str_.clear();
    _s_list_key_str_.clear();

    QFile file(m_file_path);
    if (!file.exists())
    {
        strLogBuf = QString("file not exists, file path:|%1|\n").arg(filePath);
        _s_logger_.writeLog(strLogBuf);
        return;
    }

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        strLogBuf = QString("open file failed!");
        _s_logger_.writeLog(strLogBuf);
    }
    file.close();

    int lineNumCurrentCheck;
    if (!checkFileDataIsLegal(filePath, &lineNumCurrentCheck))
    {
        strLogBuf = QString("non ascii char found, data line is:%1\n").arg(lineNumCurrentCheck);
        _s_logger_.writeLog(strLogBuf);
        emit sigOnFileFormatCheckStatus(strLogBuf);
        return;
    }

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        strLogBuf = QString("open file failed!");
        _s_logger_.writeLog(strLogBuf);
    }

    static char buf[1024];
    buf[0] = '\0';
    QStringList strListBuf;
    while (file.readLine(buf, 1024) != -1)
    {
        char *ptrCh = buf;
        QString strLineBuf = "";
        while (*ptrCh != '\0')
        {
            strLineBuf += QChar(*ptrCh);
            if (*ptrCh == '\n')
            {
                if (strLineBuf.at(0) != QChar('\r') && strLineBuf.at(0) != QChar('\n'))
                {
                    strListBuf.append(strLineBuf);
                    strLineBuf = "";
                }
            }
            ++ptrCh;
        }
    }
    file.close();

    int len = strListBuf.length();
    QString strBuf;
    for (int i = 0; i < len; ++i)
    {
        strBuf = strListBuf.at(i);
        appendHexStringLineToDataGroup(strBuf);
    }

    generateHexGroupStringFmt(_s_list_key_str_, _s_map_key_str_);

    return;
}

bool HexFormater::checkFileDataIsLegal(const QString& filePath, int *line)
{
    int len = 1;
    QString strLogBuf;

    bool isLegal = true;
    static char buf[_BUF_MAX_SIZE_];
    QFile file(filePath);
    if (!file.exists())
    {
        isLegal = false;
    }

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        strLogBuf = QString("open file failed!");
        _s_logger_.writeLog(strLogBuf);
    }

    buf[0] = 0;
    bool isFinishedCheck = false;
    while (file.readLine(buf, _BUF_MAX_SIZE_) != -1 && !isFinishedCheck)
    {
        char *chPtr = NULL;
        chPtr = buf;
        while (*chPtr != '\0')
        {
            if (*chPtr == '\r' || *chPtr == '\n')
            {
                ++chPtr;
                ++len;
                continue;
            }

            if (*chPtr >= 32 && *chPtr <= 126)
            {
                ++chPtr;
                continue;
            }
            else
            {
                isLegal = false;
                *line = len;
                isFinishedCheck = true;
                break;
            }

            ++chPtr;
        }
    }

    file.close();

    return isLegal;
}

bool HexFormater::generateHexGroupStringFmt(const QList<QString>& strListKey, const QMap<QString, QStringList*>& strMapKeyList)
{
    if (strListKey.length() > 0)
    {
        int len;
        QString strKey;
        QString strBuf;
        len = strListKey.length();
        for (int i = 0; i < len; ++i)
        {
            strKey = strListKey.at(i);
            int mode;
            if (isGroupItemKeyMapModeOne(strKey, strListKey, strMapKeyList))
            {
                generateModeOneHexStringToBuffer(strKey);
                mode = 1;
            }
            else
            {
                generateModeTwoHexStringToBuffer(strKey);
                mode = 2;
            }

            if (!_s_mode_id_.contains(strKey))
            {
                _s_mode_id_.insert(strKey, mode);
            }
        }
        formatStrDataAndSaveToLocal(m_dst_file_path, _s_str_dst_mode_);
    }

    resetListAndMapData();

    return false;
}

bool HexFormater::isGroupItemKeyMapModeOne(const QString& strKeyGroup, const QList<QString>& strListKey, const QMap<QString, QStringList*>& strMapKeyList)
{
    bool isModeOne = true;
    if (strListKey.contains(strKeyGroup) && strMapKeyList.contains(strKeyGroup))
    {
        QStringList* strList = strMapKeyList[strKeyGroup];
        if (strList->length() >= 2)
        {
            QString strGroupId, strGroupBuf;
            strGroupId = strList->at(0);
            strGroupBuf = strList->at(1);
            if (strGroupId.length() != strGroupBuf.length())
            {
                isModeOne = false;
            }
        }
    }

    return isModeOne;
}

void HexFormater::formatStrDataAndSaveToLocal(const QString& filePath, const QStringList& strDataGroupLst)
{
    QFile file(filePath);
    QTextStream stream(&file);

    if (!file.exists())
    {
        if (!file.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            QString strLog = QString("new file error, file name = |%1|.").arg(filePath);
            _s_logger_.writeLog(strLog);
            return;
        }
    }

    if (file.isOpen())
    {
        file.flush();
        file.close();
    }

    if (!file.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        QString strLog = QString("append data to file error, file name = |%1|.").arg(filePath);
        _s_logger_.writeLog(strLog);
        return;
    }

    int len = strDataGroupLst.length();
    QString strDataGroup;
    QString strSuffix;
    QString preStrKey, curStrKey;
    bool isFirst = true;
    int pre_mode = -1;
    for (int i = 0; i < len; ++i)
    {
        strDataGroup = splitStrGroupToSpecifiyWidth(strDataGroupLst.at(i));
        _s_logger_.writeLog("test:" + strDataGroup);
        curStrKey = strDataGroup;
        if (m_cur_mode == 2)
        {
            if (strDataGroup.contains("//"))
            {
                if (pre_mode == 1)
                {
                    stream << "\n" << strDataGroup << "\n" << "{ ";
                }
                else
                {
                    stream << "\n}\n" << strDataGroup << "\n" << "{ ";
                }
                preStrKey = curStrKey;
                isFirst = true;
            }
            else
            {
                if (isFirst)
                {
                    stream << "\n" << strDataGroup.toLatin1().constData();
                    isFirst = false;
                }
                else
                {
                    stream << ", \n" << strDataGroup.toLatin1().constData();
                }
            }
            pre_mode = m_cur_mode;
        }
        else
        {
            if (pre_mode == 2)
            {
                stream << "\n}\n";
            }
            stream << strDataGroup.toLatin1().constData() << "\n";
            pre_mode = m_cur_mode;
        }
        stream.flush();

        pre_mode = m_cur_mode;
    }

    stream.flush();
    file.flush();
    file.close();
}

QString HexFormater::splitStrGroupToSpecifiyWidth(const QString& strDataGroup)
{
    if (strDataGroup.contains("//"))
    {
        QString strKey = strDataGroup;
        strKey = strKey.replace("//", "#");
        if (_s_mode_id_.contains(strKey))
        {
            m_cur_mode = _s_mode_id_[strKey];
        }

        return strDataGroup;
    }

    if (m_cur_mode == 1)
    {
        QString strDataBuf = strDataGroup;
        strDataBuf = strDataBuf.replace("},", "}");
        strDataBuf = strDataBuf.replace("}, ", "}");
        strDataBuf = strDataBuf.remove("{");
        strDataBuf = strDataBuf.remove("}");
        QStringList itemLst = strDataBuf.split(",");

        QString strDstData;
        int len = itemLst.length();
        for (int i = 1; i < len; ++i)
        {
            strDstData = strDstData + itemLst.at(i);
            if ((i - 1) % 2 != 0 && i != 1 && i + 1 != len)
            {
                strDstData += ", \n    ";
            }
            else if (i + 1 != len)
            {
                strDstData += ", ";
            }
        }
        strDstData = QString("{\n    %1\n}").arg(strDstData);
        return strDstData;
    }

    const int _s_len = 20;
    QString strDataBuf = strDataGroup;
    strDataBuf.replace("},", "}");
    strDataBuf.replace("}, ", "}");
    strDataBuf = strDataBuf.remove("{");
    strDataBuf = strDataBuf.remove("}");
    strDataBuf = strDataBuf.trimmed();
    QStringList itemLst = strDataBuf.split(",");
    int len = itemLst.length();
    QString strDstBuf = "";
    QString strItem = "";
    bool isFirstItem = true;
    int prefixLenWidth;
    QString prefixStr = "";
    for (int i = 0; i < len; ++i)
    {
        if (i - 2 == 0)
        {
            prefixLenWidth = itemLst.at(0).trimmed().length() + itemLst.at(1).trimmed().length() + 10;
            for (int j = 0; j < prefixLenWidth; ++j)
            {
                prefixStr += " ";
            }
        }

        if (!isFirstItem && (i - 3 > 0) && (i - 3) % _s_len == 0)
        {
            strDstBuf += "\n" + prefixStr;
        }

        if (isFirstItem && (i - 3 > 0) && (i - 3) % _s_len == 0)
        {
            prefixLenWidth = strDstBuf.length();
            strDstBuf += "\n" + prefixStr;
            isFirstItem = false;
        }

        strItem = itemLst.at(i).trimmed();
        if (i + 1 != len)
        {
            strDstBuf += strItem + ", ";
        }
        else
        {
            strDstBuf += strItem;
        }
    }

    return strDstBuf;
}

QStringList& HexFormater::mergeSortGroupByData(const QStringList& strFullEQList, const QStringList& strPartEQList)
{
    static QStringList strDataList;

    strDataList.clear();
    QString strFLine, strPLine;
    QStringList fItemList = strFullEQList, pItemList = strPartEQList;

    //Queue logic
    while (!fItemList.isEmpty())
    {
        strFLine = fItemList.front();
        strPLine = pItemList.front();
        fItemList.pop_front();
        if (pItemList.isEmpty())
        {
            strDataList.append(strFLine);
        }
        else if (isFrontThreeItemDiffer(strFLine, strPLine))
        {
            strDataList.append(strFLine);
        }
        else if (!isFrontThreeItemDiffer(strFLine, strPLine)) //same
        {
            strDataList.append(strPLine);
            pItemList.pop_front();
        }
    }

    return strDataList;
}

bool HexFormater::isFrontThreeItemDiffer(const QString& strA, const QString& strB) const
{
    bool isDiffer = false;
    QStringList strListA = strA.split(",");
    QStringList strListB = strB.split(",");

    if (strListA.length() != strListB.length())
    {
        isDiffer = true;
    }

    if (strListA.length() >= 3 && strListB.length() >= 3)
    {
        if (strListA.at(0).trimmed() == strListB.at(0).trimmed() &&
                strListA.at(1).trimmed() == strListB.at(1).trimmed() &&
                strListA.at(2).trimmed() == strListB.at(2).trimmed())
        {
            isDiffer = false;
        }
        else
        {
            isDiffer = true;
        }
    }

    return isDiffer;
}
